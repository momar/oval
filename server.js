var express = require("express");
var request = require("request");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server);
server.listen(8080);

app.use(express.static("web"));
app.use("/proxy/", (req, res) => {
    req.pipe(request(req.url.substr(1))).pipe(res);
});

var widgets = () => require("js-yaml").safeLoad(require("fs").readFileSync("widgets.yaml", "utf-8"));

var visible = {}, values = {};
io.on("connection", s => {
    s.emit("config", widgets().map(x => ({...x, visible: !!visible[x.name], value: values[x.name]})));
    s.on("show", x => { visible[x.name] = true; io.emit("show", x); });
    s.on("hide", x => { visible[x.name] = false; io.emit("hide", x); });
    s.on("update", x => { values[x.name] = x.value; io.emit("update", x); });
});
